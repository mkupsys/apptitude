     
var latitude, longitude, link, shortLink;
var urls ={queue: []};
localStorage.setItem('urls', JSON.stringify(urls));
 
myFunction = () => {
    longitude = parseFloat((Math.random()*360-180).toFixed(8));
    latitude = parseFloat((Math.random()*180-90).toFixed(8));
    document.getElementById("long").innerHTML = longitude;
    document.getElementById("lat").innerHTML = latitude;
    link = `https://www.google.lt/maps/@${longitude},${latitude},14z?hl=en`;
    linkToDom = `<a target="_blank" href="https://www.google.lt/maps/@${longitude},${latitude},14z?hl=en">https://www.google.lt/maps/@${longitude},${latitude},14z?hl=en</a>`
    document.getElementById("link").innerHTML = linkToDom;
};
myFunction();

display = () => {
var getUrl = JSON.parse(localStorage.getItem('urls'));
var outputs = "";
if (getUrl.queue.length === 0){
    outputs = "Local storage is empty!"
}else {
for(var i = 0; i < getUrl.queue.length; i++)
    {
    outputs += `<div><span>${[i+1]}.</span>
    <a target="_blank" href="https://${getUrl.queue[i].name}">${getUrl.queue[i].name}</a target="_blank href="></div>`;
    }  
}    
document.getElementById("demo").innerHTML= outputs;
}
display();

function initMap() {           
var coordinates = {lat: latitude, lng: longitude};

  
var map = new google.maps.Map(document.getElementById('map'), {zoom: 4, center: coordinates});

var marker = new google.maps.Marker({position: coordinates, map: map});

}; 
function startTimer(duration, display) {
    var timer = duration, seconds;
        setInterval( () => {        
        seconds = parseInt(timer % 60, 10);        
        seconds = seconds < 10 ? "0" + seconds : seconds;        
        display.textContent = seconds;
        if (--timer < 0) {
            timer = duration;
            myFunction();
            shortURL();
            initMap();
        }
    }, 1000);       
}

window.onload = function () {
    const duration = 30,
        display = document.querySelector('#time');
    startTimer(duration, display);
}
  

pushIt = () => {
    // var cookieValue = document.getElementById("result").innerHTML;    
    var geturls  = JSON.parse(localStorage.getItem('urls'));  
    geturls.queue.push({        
        name: shortLink
    });    
    localStorage.setItem('urls', JSON.stringify(geturls));
    display();
}


// Short URL
 shortURL = async () => {
    var params = {
        destination: link,
        domain: { fullName: "rebrand.ly" }
        }
    const rawResponse = await fetch('https://api.rebrandly.com/v1/links', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      "apikey": "90ffee00f0c2469885e2b3fc3e2883bb"
    },
    body: JSON.stringify(params)
  });
  const content = await rawResponse.json();
  shortLink = content.shortUrl;
  document.getElementById("result").innerHTML = linkToDom = `<a target="_blank" href="https://${shortLink}">${shortLink}</a>`  
};
shortURL();

